//
//  BCItemList.h
//  BCItemList
//
//  Created by Chung BD on 7/3/15.
//  Copyright (c) 2015 Bui Chung. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BCItemList : UIView
@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) NSMutableArray *customConstraints;
@end
